//
//  CGPath+.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 17/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

extension CGPath {
    
    func translate(by point: CGPoint) -> CGPath {
        let bezierPath = UIBezierPath(cgPath: self)
        let transform = CGAffineTransform(translationX: point.x, y: point.y)
        bezierPath.apply(transform)
        return bezierPath.cgPath
    }
    
    func scale(by point: CGPoint) -> CGPath {
        let bezierPath = UIBezierPath(cgPath: self)
        let transform = CGAffineTransform(scaleX: point.x, y: point.y)
        bezierPath.apply(transform)
        return bezierPath.cgPath
    }
}
