//
//  UITableView+.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 14/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

extension UITableView {
    func register<T: UITableViewCell>(cell: T.Type) {
        let nibName = String(describing: cell)
        let reuseIdentifier = String(describing: cell)
        if Bundle.main.path(forResource: nibName, ofType: "nib") != nil {
            register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        } else {
            register(cell, forCellReuseIdentifier: reuseIdentifier)
        }
    }
    
    func dequeue<T: UITableViewCell>(cell: T.Type, at indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: String(describing: cell), for: indexPath) as! T
    }
}

extension UICollectionView {
    func register<T: UICollectionViewCell>(cell: T.Type) {
        let nibName = String(describing: cell)
        let reuseIdentifier = String(describing: cell)
        if Bundle.main.path(forResource: nibName, ofType: "nib") != nil {
            register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        } else {
            register(cell, forCellWithReuseIdentifier: reuseIdentifier)
        }
    }
    
    func dequeue<T: UICollectionViewCell>(cell: T.Type, at indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: String(describing: cell), for: indexPath) as! T
    }
}
