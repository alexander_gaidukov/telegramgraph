//
//  Date+.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 17/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import Foundation

extension Date {
    func days(to date: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: self, to: date)
        return components.day ?? 0
    }
    
    func hours(to date: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.hour], from: self, to: date)
        return components.hour ?? 0
    }
}
