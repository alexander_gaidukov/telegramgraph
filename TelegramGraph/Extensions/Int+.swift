//
//  Int+.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 10/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import Foundation

extension Int {
    var stringRepresentation: String {
        if self < 1_000 {
            return "\(self)"
        }
        
        if self < 1_000_000 {
            let value = Double(self) / 1_000.0
            return "\(Constants.numberFormatter.string(from: NSNumber(value: value))!)K"
        }
        
        let value = Double(self) / 1_000_000.0
        return "\(Constants.numberFormatter.string(from: NSNumber(value: value))!)M"
    }
    
    var groupingRepresentation: String {
        return Constants.groupingNumberFormatter.string(from: NSNumber(value: self))!
    }
}
