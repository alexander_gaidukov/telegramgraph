//
//  Constants.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 18/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit


enum Theme: Int {
    case day = 0
    case night = 1
}

extension Theme {
    var cellBackgroundColor: UIColor {
        switch self {
        case .day:
            return .white
        case .night:
            return UIColor(hex: "#212f3f")
        }
    }
    
    var tableViewBackgroundColor: UIColor {
        switch self {
        case .day:
            return UIColor(hex: "#EFEFF4")
        case .night:
            return UIColor(hex: "#18222d")
        }
    }
    
    var navbarBackgroundColor: UIColor {
        switch self {
        case .day:
            return UIColor(hex: "#F7F7F7")
        case .night:
            return UIColor(hex: "#212f3f")
        }
    }
    
    var separatorColor: UIColor {
        switch self {
        case .day:
            return UIColor(hex: "#182D3B").withAlphaComponent(0.1)
        case .night:
            return UIColor(hex: "#8596AB").withAlphaComponent(0.2)
        }
    }
    
    var sliderBackgroundColor: UIColor {
        switch self {
        case .day:
            return UIColor(hex: "#E2EEF9").withAlphaComponent(0.6)
        case .night:
            return UIColor(hex: "#18222D").withAlphaComponent(0.6)
        }
    }
    
    var sliderWindowColor: UIColor {
        switch self {
        case .day:
            return UIColor(hex: "#C0D1E1")
        case .night:
            return UIColor(hex: "#56626D")
        }
    }
    
    var titleColor: UIColor {
        switch self {
        case .day:
            return .darkText
        case .night:
            return .white
        }
    }
    
    var lightenMask: UIColor {
        switch self {
        case .day:
            return UIColor.white.withAlphaComponent(0.5)
        case .night:
            return UIColor(hex:"#212F3F").withAlphaComponent(0.5)
        }
    }
    
    var tooltipArrowColor: UIColor {
        switch self {
        case .day:
            return UIColor(hex:"#59606D").withAlphaComponent(0.3)
        case .night:
            return UIColor(hex:"#D2D5D7")
        }
    }
    
    var switchText: String {
        switch self {
        case .day:
            return "Night Mode"
        case .night:
            return "Day Mode"
        }
    }
    
    var switched: Theme {
        switch self {
        case .day:
            return .night
        case .night:
            return .day
        }
    }
    
    mutating func toggle() {
        self = switched
    }
}

struct Constants {
    
    private static let themeKey = "graph_theme"
    
    static var theme: Theme = Theme(rawValue: UserDefaults.standard.integer(forKey: Constants.themeKey))! {
        didSet {
            UserDefaults.standard.set(theme.rawValue, forKey: Constants.themeKey)
            UserDefaults.standard.synchronize()
        }
    }
    static let animationDuration: TimeInterval = 0.3
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.dateFormat = "MMM dd"
        return formatter
    }()
    
    static let calloutDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.dateFormat = "EEE, dd MMM yyyy"
        return formatter
    }()
    
    static let calloutHourlyDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.dateFormat = "dd MMM HH:mm"
        return formatter
    }()
    
    static let dateRangesDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
    }()
    
    static let zoomDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.dateFormat = "yyyy-MM dd"
        return formatter
    }()
    
    static let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")!
        formatter.dateFormat = "HH:mm"
        return formatter
    }()
    
    static let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    static var numberFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 4
        return formatter
    }
    
    static var groupingNumberFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = " "
        formatter.numberStyle = .decimal
        return formatter
    }
}
