//
//  LeftAlignedCollectionViewFlowLayout.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 10/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        
        attributes?.forEach {
            if $0.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            $0.frame.origin.x = leftMargin
            leftMargin += $0.frame.width + minimumInteritemSpacing
            maxY = max($0.frame.maxY, maxY)
        }
        
        return attributes
    }
}
