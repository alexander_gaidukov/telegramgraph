//
//  PercentageAreaGraphModel.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 10/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class PercentageAreaGraphModel: GraphViewModel {
    var graph: Graph
    var hiddenColumns:Set<Int> = [] {
        didSet {
            calculateRelativeValues()
            updateThumbValues()
            updateMainPlotValues()
        }
    }
    var leftOffset: CGFloat = 0.5
    var windowWidth: CGFloat = 0.5
    
    private let folder: String
    
    init(graph: Graph, folder: String) {
        self.graph = graph
        self.folder = folder
        calculateRelativeValues()
        updateDatesRange()
        updateThumbValues()
        updateMainPlotValues()
    }
    
    var startDate: Date!
    var endDate: Date!
    var hourly: Bool = false
    
    var coordinateValues: (left: [(color: UIColor, value: Int)]?, right: [(color: UIColor, value: Int)]?) {
        let textColor = UIColor(hex: "#252529").withAlphaComponent(0.5)
        let step = 100 / linesCount
        let leftValues: [(color: UIColor, value: Int)] = (0..<linesCount).map {
            return (color: textColor, value: step * $0)
        }
        return (left: leftValues, right: nil)
    }
    
    private var points: [[CGPoint]] = []
    private var heightScales: [CGFloat] = []
    private var thumbPoints: [[CGPoint]] = []
    private var thumbHeightScales: [CGFloat] = []
    
    private var relativeValues: [[Double]] = []
    
    private func calculateRelativeValues() {
        var relativeValues:[[Int]] = []
        for (index, column) in graph.lineColumns.enumerated() {
            guard !hiddenColumns.contains(index) else { continue }
            let values = column.values
            if let lastValues = relativeValues.last {
                let sumValues = zip(lastValues, values).map { $0 + $1 }
                relativeValues.append(sumValues)
            } else {
                relativeValues.append(values)
            }
        }
        
        let dividers = relativeValues.last!
        
        self.relativeValues = relativeValues.map { values in
            zip(values, dividers).map { Double($0) / Double($1) }
        }
    }
    
    private func updateThumbValues() {
        thumbPoints = calculatePoints(isThumb: true)
        thumbHeightScales = calculateScales(isThumb: true)
    }
    
    private func updateMainPlotValues() {
        points = calculatePoints(isThumb: false)
        heightScales = calculateScales(isThumb: false)
    }
    
    func windowDidChange() {
        
    }
    
    func zoomedViewModel(forDate date: Date) -> GraphViewModel? {
        return PieChartGraphModel(graph: self.graph, hiddenColumns: self.hiddenColumns, leftOffset: self.leftOffset, windowWidth: self.windowWidth, relativeValues: self.relativeValues, thumbPoints: self.thumbPoints, thumbScales: self.thumbHeightScales, startDate: self.startDate, endDate: self.endDate)
    }
    
    func plotLocationValues(at index: Int) -> [PlotLocationValue] {
        var idx = 0
        var prevPercent: Int = 0
        return graph.lineColumns.enumerated().map {
            guard !self.hiddenColumns.contains($0.offset) else {
                return (name: $0.element.name, color: $0.element.color, value: nil)
            }
            let currentPercent = Int(round((relativeValues[idx][index]) * 100.0))
            let percent = currentPercent - prevPercent
            prevPercent = currentPercent
            idx += 1
            return (name: "\(percent)% \($0.element.name!)", color: $0.element.color!, value: $0.element.values[index])
        }
    }

    func getYRanges(isThumb: Bool) -> [(min: Int, max: Int)] {
        return Array(repeating: (min: 0, max: 100), count: graph.lineColumns.count)
    }
    
    func getHeightScales(isThumb: Bool) -> [CGFloat] {
        return isThumb ? thumbHeightScales : heightScales
    }
    
    func getPoints(isThumb: Bool) ->[[CGPoint]] {
        return isThumb ? thumbPoints : points
    }
    
    private func updateDatesRange() {
        let xColumn = graph.xColumn
        startDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.min()!) / 1000.0)
        endDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.max()!) / 1000.0)
    }
    
    private func calculateScales(isThumb: Bool) -> [CGFloat] {
        
        var scales: [CGFloat] = []
        var index = 0
        for idx in 0..<graph.lineColumns.count {
            guard !hiddenColumns.contains(idx) else {
                scales.append(0)
                continue
            }
            let scale = CGFloat(relativeValues[index].max()!)
            scales.append(scale)
            index += 1
        }
        
        return scales
    }
    
    private func calculatePoints(isThumb: Bool) -> [[CGPoint]] {
        
        let xValues = graph.xColumn.values
        
        let maxX = xValues.max()!
        let minX = xValues.min()!
        
        let xDistance = maxX - minX
        
        var points: [[CGPoint]] = Array(repeating: [], count: graph.lineColumns.count)
        
        for (i, xValue) in xValues.enumerated() {
            let x = CGFloat(xValue - minX) / CGFloat(xDistance)
            var index = 0
            for j in 0..<graph.lineColumns.count {
                guard !hiddenColumns.contains(j) else {
                    points[j].append(CGPoint(x: x, y: 0))
                    continue
                }
                let maxY = relativeValues[index].max()!
                let y = CGFloat(relativeValues[index][i]) / CGFloat(maxY)
                let point = CGPoint(x: x, y: y)
                points[j].append(point)
                index += 1
            }
        }
        
        return points
    }
}
