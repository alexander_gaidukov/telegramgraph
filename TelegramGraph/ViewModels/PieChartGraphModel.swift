//
//  PieChartGraphModel.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 13/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class PieChartGraphModel: GraphViewModel {
    var graph: Graph
    var hiddenColumns:Set<Int> = [] {
        didSet {
            calculateRelativeValues()
            updateThumbValues()
            updateMainPlotValues()
        }
    }
    
    var leftOffset: CGFloat = 0.5
    var windowWidth: CGFloat = 0.5
    var startDate: Date!
    var endDate: Date!
    var hourly: Bool = false
    private var heightScales: [CGFloat] = []
    private var thumbPoints: [[CGPoint]] = []
    private var thumbHeightScales: [CGFloat] = []
    private var relativeValues: [[Double]] = []
    private let folder: String
    
    init(graph: Graph, folder: String) {
        self.graph = graph
        self.folder = folder
        calculateRelativeValues()
        updateDatesRange()
        updateThumbValues()
        updateMainPlotValues()
    }
    
    init(graph: Graph, hiddenColumns: Set<Int>, leftOffset: CGFloat, windowWidth: CGFloat, relativeValues: [[Double]], thumbPoints: [[CGPoint]], thumbScales:[CGFloat], startDate: Date, endDate: Date) {
        self.graph = graph
        self.hiddenColumns = hiddenColumns
        self.leftOffset = leftOffset
        self.windowWidth = windowWidth
        self.relativeValues = relativeValues
        self.thumbPoints = thumbPoints
        self.thumbHeightScales = thumbScales
        self.startDate = startDate
        self.endDate = endDate
        self.folder = ""
        updateMainPlotValues()
    }
    
    var coordinateValues: (left: [(color: UIColor, value: Int)]?, right: [(color: UIColor, value: Int)]?) {
        return (left: nil, right: nil)
    }
    
    private func calculateRelativeValues() {
        var relativeValues:[[Int]] = []
        for (index, column) in graph.lineColumns.enumerated() {
            guard !hiddenColumns.contains(index) else { continue }
            let values = column.values
            if let lastValues = relativeValues.last {
                let sumValues = zip(lastValues, values).map { $0 + $1 }
                relativeValues.append(sumValues)
            } else {
                relativeValues.append(values)
            }
        }
        
        let dividers = relativeValues.last!
        
        self.relativeValues = relativeValues.map { values in
            zip(values, dividers).map { Double($0) / Double($1) }
        }
    }
    
    private func calculateRelativeValuesForVisibleRect() -> [CGFloat] {
        let elementsCount = graph.xColumn.values.count
        let firstIndex = Int(floor(leftOffset * CGFloat(elementsCount)))
        let lastIndex = min(Int(ceil((leftOffset + windowWidth) * CGFloat(elementsCount))), elementsCount - 1)
        var result: [CGFloat] = []
        var prevAvgValue: Double = 0
        for columnValues in relativeValues {
            let avgValue = round(columnValues[firstIndex...lastIndex].reduce(0, +) / Double(lastIndex - firstIndex + 1) * 100.0) / 100.0
            result.append(CGFloat(avgValue - prevAvgValue))
            prevAvgValue = avgValue
        }
        
        var scales: [CGFloat] = []
        var index = 0
        for idx in 0..<graph.lineColumns.count {
            guard !hiddenColumns.contains(idx) else {
                scales.append(0)
                continue
            }
            let scale = CGFloat(result[index])
            scales.append(scale)
            index += 1
        }
        
        return scales
    }
    
    private func updateThumbValues() {
        thumbPoints = calculateThumbPoints()
        thumbHeightScales = calculateScales()
    }
    
    private func updateMainPlotValues() {
        heightScales = calculateRelativeValuesForVisibleRect()
    }
    
    func windowDidChange() {
        updateMainPlotValues()
    }
    
    func zoomedViewModel(forDate date: Date) -> GraphViewModel? {
        return nil
    }
    
    func plotLocationValues(at index: Int) -> [PlotLocationValue] {
        return []
    }
    
    func getYRanges(isThumb: Bool) -> [(min: Int, max: Int)] {
        return Array(repeating: (min: 0, max: 100), count: graph.lineColumns.count)
    }
    
    func getHeightScales(isThumb: Bool) -> [CGFloat] {
        return isThumb ? thumbHeightScales : heightScales
    }
    
    func getPoints(isThumb: Bool) ->[[CGPoint]] {
        return isThumb ? thumbPoints : []
    }
    
    private func updateDatesRange() {
        let xColumn = graph.xColumn
        startDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.min()!) / 1000.0)
        endDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.max()!) / 1000.0)
    }
    
    private func calculateScales() -> [CGFloat] {
        var scales: [CGFloat] = []
        var index = 0
        for idx in 0..<graph.lineColumns.count {
            guard !hiddenColumns.contains(idx) else {
                scales.append(0)
                continue
            }
            let scale = CGFloat(relativeValues[index].max()!)
            scales.append(scale)
            index += 1
        }
        return scales
    }
    
    private func calculateThumbPoints() -> [[CGPoint]] {
        
        let xValues = graph.xColumn.values
        
        let maxX = xValues.max()!
        let minX = xValues.min()!
        
        let xDistance = maxX - minX
        
        var points: [[CGPoint]] = Array(repeating: [], count: graph.lineColumns.count)
        
        for (i, xValue) in xValues.enumerated() {
            let x = CGFloat(xValue - minX) / CGFloat(xDistance)
            var index = 0
            for j in 0..<graph.lineColumns.count {
                guard !hiddenColumns.contains(j) else {
                    points[j].append(CGPoint(x: x, y: 0))
                    continue
                }
                let maxY = relativeValues[index].max()!
                let y = CGFloat(relativeValues[index][i]) / CGFloat(maxY)
                let point = CGPoint(x: x, y: y)
                points[j].append(point)
                index += 1
            }
        }
        
        return points
    }
}
