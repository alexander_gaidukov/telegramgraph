//
//  GraphViewModel.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 14/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol GraphViewModel {
    var graph: Graph { get }
    var hourly: Bool { get }
    var hasThumb: Bool { get }
    var hiddenColumns: Set<Int> { get set }
    var coordinateValues: (left: [(color: UIColor, value: Int)]?, right: [(color: UIColor, value: Int)]?) { get }
    var leftOffset: CGFloat { get set }
    var windowWidth: CGFloat { get set }
    var startDate: Date! { get }
    var endDate: Date! { get }
    init(graph: Graph, folder: String)
    func getYRanges(isThumb: Bool) -> [(min: Int, max: Int)]
    func getHeightScales(isThumb: Bool) -> [CGFloat]
    func getPoints(isThumb: Bool) ->[[CGPoint]]
    func windowDidChange()
    func plotLocationValues(at index: Int) -> [PlotLocationValue]
    func zoomedViewModel(forDate date: Date) -> GraphViewModel?
}

extension GraphViewModel {
    
    func dateForTimeline(at index: Int) -> String {
        let multiplier = hourly ? 3600 : 24 * 3600
        let date = startDate.addingTimeInterval(TimeInterval(multiplier * index))
        let formatter = hourly ? Constants.timeFormatter : Constants.dateFormatter
        return formatter.string(from: date)
    }
    
    var daysCount: Int {
        return hourly ? startDate.hours(to: endDate) : startDate.days(to: endDate)
    }
    
    var linesCount: Int {
        return 5
    }
    
    var canHideColumns: Bool {
        return graph.lineColumns.count - hiddenColumns.count > 1
    }
    
    var notHiddenColumns: [Int] {
        return (0..<graph.lineColumns.count).filter { !hiddenColumns.contains($0) }
    }
    
    var hasThumb: Bool {
        return true
    }
    
    var displayedDateRanges: (start: Date, end: Date) {
        let startIndex = Int(ceil(leftOffset * CGFloat(daysCount)))
        let endIndex = Int(floor((leftOffset + windowWidth) * CGFloat(daysCount)))
        let multiplier = hourly ? 3600 : 24 * 3600
        return (start: startDate.addingTimeInterval(TimeInterval(multiplier * startIndex)), end: startDate.addingTimeInterval(TimeInterval(multiplier * endIndex)))
    }
    
    func plotLocationValues(at index: Int) -> [PlotLocationValue] {
        return graph.lineColumns.enumerated().map {
            let value = self.hiddenColumns.contains($0.offset) ? nil :  $0.element.values[index]
            return (name: $0.element.name, color: $0.element.color!, value: value)
        }
    }
    
    func date(at index: Int) -> Date {
        let multiplier = hourly ? 3600 : 24 * 3600
        return startDate.addingTimeInterval(TimeInterval(index * multiplier))
    }
}
