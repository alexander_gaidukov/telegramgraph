//
//  StackedBarGraphViewModel.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 09/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class StackedBarGraphViewModel: GraphViewModel {
    
    var graph: Graph
    var hiddenColumns:Set<Int> = [] {
        didSet {
            calculateRelativeValues()
            updateThumbValues()
            updateMainPlotValues()
        }
    }
    var leftOffset: CGFloat = 0.5
    var windowWidth: CGFloat = 0.5
    
    private var yRanges: (min: Int, max: Int) = (min: 0, max: 0)
    private var thumbYRanges: (min: Int, max: Int) = (min: 0, max: 0)
    
    private let folder: String
    
    init(graph: Graph, folder: String) {
        self.graph = graph
        self.folder = folder
        calculateRelativeValues()
        updateDatesRange()
        updateThumbValues()
        updateMainPlotValues()
    }
    
    init(graph: Graph, leftOffset: CGFloat, windowWidth: CGFloat, hiddenColumns: Set<Int>) {
        self.graph = graph
        self.folder = ""
        self.hourly = true
        self.leftOffset = leftOffset
        self.windowWidth = windowWidth
        self.hiddenColumns = hiddenColumns
        calculateRelativeValues()
        updateDatesRange()
        updateThumbValues()
        updateMainPlotValues()
    }
    
    var startDate: Date!
    var endDate: Date!
    var hourly: Bool = false
    
    var coordinateValues: (left: [(color: UIColor, value: Int)]?, right: [(color: UIColor, value: Int)]?) {
        let textColor = UIColor(hex: "#252529").withAlphaComponent(0.5)
        let step = yRanges.max / linesCount
        let leftValues: [(color: UIColor, value: Int)] = (0..<linesCount).map {
            return (color: textColor, value: step * $0)
        }
        return (left: leftValues, right: nil)
    }
    
    private var points: [[CGPoint]] = []
    private var heightScales: [CGFloat] = []
    private var thumbPoints: [[CGPoint]] = []
    private var thumbHeightScales: [CGFloat] = []
    
    private var relativeValues: [[Int]] = []
    
    private func calculateRelativeValues() {
        var relativeValues: [[Int]] = []
        for (index, column) in graph.lineColumns.enumerated() {
            guard !hiddenColumns.contains(index) else { continue }
            let values = column.values
            if let lastValues = relativeValues.last {
                let sumValues = zip(lastValues, values).map { $0 + $1 }
                relativeValues.append(sumValues)
            } else {
                relativeValues.append(values)
            }
        }
        self.relativeValues = relativeValues
    }
    
    private func updateThumbValues() {
        updateThumbYRanges()
        thumbPoints = calculatePoints(isThumb: true)
        thumbHeightScales = calculateScales(isThumb: true)
    }
    
    private func updateMainPlotValues() {
        updateYRanges()
        points = calculatePoints(isThumb: false)
        heightScales = calculateScales(isThumb: false)
    }
    
    func windowDidChange() {
        updateMainPlotValues()
    }
    
    func zoomedViewModel(forDate date: Date) -> GraphViewModel? {
        let calendar = Calendar.current
        var dayIndex = calendar.component(.weekday, from: date) - 2
        if dayIndex < 0 {
            dayIndex = 6
        }
        let startDate = max(self.startDate, date.addingTimeInterval(-TimeInterval(24 * 3600 * dayIndex)))
        let endDate = min(self.endDate, date.addingTimeInterval(TimeInterval(24 * 3600 * (6 - dayIndex))))
        let days = startDate.days(to: endDate) + 1
        
        var result: Graph! = nil
        
        for index in 0..<days {
            let currDate = startDate.addingTimeInterval(TimeInterval(24 * 3600 * index))
            let dateStr = Constants.zoomDateFormatter.string(from: currDate)
            let dateComponents = dateStr.components(separatedBy: " ")
            let folderPath = folder + "/" + dateComponents.first!
            let url = Bundle.main.url(forResource: dateComponents.last!, withExtension: "json", subdirectory: folderPath)!
            let data = try! Data(contentsOf: url)
            let graph = try! Constants.decoder.decode(Graph.self, from: data)
            if index == 0 {
                result = graph
            } else {
                var columns = result.columns
                for (index, column) in graph.columns.enumerated() {
                    columns[index].values += column.values
                }
                result.columns = columns
            }
        }
        
        let windowWidth = CGFloat(1.0) / CGFloat(days)
        let leftOffset = CGFloat(startDate.days(to: date)) / CGFloat(days)
        
        return StackedBarGraphViewModel(graph: result, leftOffset: leftOffset, windowWidth:windowWidth, hiddenColumns: self.hiddenColumns)
    }
    
    func getYRanges(isThumb: Bool) -> [(min: Int, max: Int)] {
        let ranges = isThumb ? thumbYRanges : yRanges
        return Array(repeating: ranges, count: graph.lineColumns.count)
    }
    
    func getHeightScales(isThumb: Bool) -> [CGFloat] {
        return isThumb ? thumbHeightScales : heightScales
    }
    
    func getPoints(isThumb: Bool) ->[[CGPoint]] {
        return isThumb ? thumbPoints : points
    }
    
    private func boundValuesFor(minEl: Int, maxEl: Int) -> (min: Int, max: Int) {
        let diff = maxEl - minEl
        let numberOfDigits = max(String(describing: diff).count - 2, 0)
        let multiplier = Int(round(pow(10.0, Double(numberOfDigits))))
        let minYBound = 0
        let way = maxEl - minYBound
        let step = Double(way) / Double(linesCount)
        let maxYBound = minYBound + Int(ceil(Double(step / Double(multiplier)))) * multiplier * linesCount
        return (min: minYBound, max: maxYBound)
    }
    
    private func updateYRanges() {
        let elementsCount = graph.xColumn.values.count
        let firstIndex = Int(floor(leftOffset * CGFloat(elementsCount)))
        let lastIndex = min(Int(ceil((leftOffset + windowWidth) * CGFloat(elementsCount))), elementsCount - 1)
        let maxY = relativeValues.last![firstIndex...lastIndex].max()!
        yRanges = boundValuesFor(minEl: 0, maxEl: maxY)
    }
    
    private func updateThumbYRanges() {
        let maxY = relativeValues.last!.max()!
        yRanges = boundValuesFor(minEl: 0, maxEl: maxY)
    }
    
    private func updateDatesRange() {
        let xColumn = graph.xColumn
        startDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.min()!) / 1000.0)
        endDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.max()!) / 1000.0)
    }
    
    private func calculateScales(isThumb: Bool) -> [CGFloat] {
        
        var scales: [CGFloat] = []
        var index = 0
        for idx in 0..<graph.lineColumns.count {
            guard !hiddenColumns.contains(idx) else {
                scales.append(0)
                continue
            }
            let scale = CGFloat(relativeValues[index].max()!) / CGFloat(yRanges.max)
            scales.append(scale)
            index += 1
        }
        
        return scales
    }
    
    private func calculatePoints(isThumb: Bool) -> [[CGPoint]] {
        
        let xValues = graph.xColumn.values
        
        let maxX = xValues.max()!
        let minX = xValues.min()!
        
        let xDistance = maxX - minX + 1
        
        var points: [[CGPoint]] = Array(repeating: [], count: graph.lineColumns.count)
        
        for (i, xValue) in xValues.enumerated() {
            let x = CGFloat(xValue - minX) / CGFloat(xDistance)
            var index = 0
            for j in 0..<graph.lineColumns.count {
                guard !hiddenColumns.contains(j) else {
                    points[j].append(CGPoint(x: x, y: 0))
                    continue
                }
                let maxY = relativeValues[index].max()!
                let y = CGFloat(relativeValues[index][i]) / CGFloat(maxY)
                let point = CGPoint(x: x, y: y)
                points[j].append(point)
                index += 1
            }
        }
        
        return points
    }
}
