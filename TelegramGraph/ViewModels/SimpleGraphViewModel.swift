//
//  SimpleGraphViewModel.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 09/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class SimpleGraphViewModel: GraphViewModel {
    
    var graph: Graph
    var hiddenColumns:Set<Int> = [] {
        didSet {
            updateThumbValues()
            updateMainPlotValues()
        }
    }
    var leftOffset: CGFloat = 0.5
    var windowWidth: CGFloat = 0.5
    
    var hasThumb: Bool = true
    
    private let folder: String
    var hourly: Bool = false
    
    private var yRanges: (min: Int, max: Int) = (min: 0, max: 0)
    private var thumbYRanges: (min: Int, max: Int) = (min: 0, max: 0)
    
    init(graph: Graph, folder: String) {
        self.graph = graph
        self.folder = folder
        updateDatesRange()
        updateThumbValues()
        updateMainPlotValues()
    }
    
    init(graph: Graph, leftOffset: CGFloat, windowWidth: CGFloat, hiddenColumns: Set<Int>) {
        self.graph = graph
        self.folder = ""
        self.hourly = true
        self.leftOffset = leftOffset
        self.windowWidth = windowWidth
        self.hiddenColumns = hiddenColumns
        updateDatesRange()
        updateThumbValues()
        updateMainPlotValues()
    }
    
    var startDate: Date!
    var endDate: Date!
    
    var coordinateValues: (left: [(color: UIColor, value: Int)]?, right: [(color: UIColor, value: Int)]?) {
        let textColor = UIColor(hex: "#8E8E93")
        
        let step = (yRanges.max - yRanges.min) / linesCount
        let leftValues: [(color: UIColor, value: Int)] = (0..<linesCount).map {
            return (color: textColor, value: yRanges.min + step * $0)
        }
        return (left: leftValues, right: nil)
    }
    
    private var points: [[CGPoint]] = []
    private var heightScales: [CGFloat] = []
    private var thumbPoints: [[CGPoint]] = []
    private var thumbHeightScales: [CGFloat] = []
    
    private func updateThumbValues() {
        let prevMinYValues = thumbYRanges.min
        updateThumbYRanges()
        if prevMinYValues != thumbYRanges.min || thumbPoints.count == 0 {
            thumbPoints = calculatePoints(isThumb: true)
        }
        thumbHeightScales = calculateScales(isThumb: true)
    }
    
    private func updateMainPlotValues() {
        let prevMinYValues = yRanges.min
        updateYRanges()
        if prevMinYValues != yRanges.min || points.count == 0 {
            points = calculatePoints(isThumb: false)
        }
        heightScales = calculateScales(isThumb: false)
    }
    
    func singleValueZoomedViewModel(forDate date: Date) -> GraphViewModel {
        let dates = [max(startDate,date.addingTimeInterval(-TimeInterval(7 * 24 * 3600))), max(startDate, date.addingTimeInterval(-TimeInterval(24 * 3600))), date]
        let colors = [UIColor(hex: "#3381E8"), UIColor(hex:"#77BAF4"), UIColor(hex: "#74DAFF")]
        var result: Graph! = nil
        
        for index in 0..<dates.count {
            let currDate = dates[index]
            let dateStr = Constants.zoomDateFormatter.string(from: currDate)
            let dateComponents = dateStr.components(separatedBy: " ")
            let folderPath = folder + "/" + dateComponents.first!
            let url = Bundle.main.url(forResource: dateComponents.last!, withExtension: "json", subdirectory: folderPath)!
            let data = try! Data(contentsOf: url)
            let graph = try! Constants.decoder.decode(Graph.self, from: data)
            if index == 0 {
                result = graph
                result.columns[1].label = "y0"
                result.columns[1].name = Constants.dateFormatter.string(from: currDate)
                result.columns[1].color = colors[index]
            } else {
                var column = graph.columns[1]
                column.label = "y\(index)"
                column.name = Constants.dateFormatter.string(from: currDate)
                column.color = colors[index]
                result.columns.append(column)
            }
        }
        
        let model = SimpleGraphViewModel(graph: result, leftOffset: 0.0, windowWidth:1.0, hiddenColumns: self.hiddenColumns)
        model.hasThumb = false
        return model
    }
    
    func zoomedViewModel(forDate date: Date) -> GraphViewModel? {
        guard graph.lineColumns.count > 1 else { return singleValueZoomedViewModel(forDate:date) }
        let calendar = Calendar.current
        var dayIndex = calendar.component(.weekday, from: date) - 2
        if dayIndex < 0 {
            dayIndex = 6
        }
        let startDate = max(self.startDate, date.addingTimeInterval(-TimeInterval(24 * 3600 * dayIndex)))
        let endDate = min(self.endDate, date.addingTimeInterval(TimeInterval(24 * 3600 * (6 - dayIndex))))
        let days = startDate.days(to: endDate) + 1
        
        var result: Graph! = nil
        
        for index in 0..<days {
            let currDate = startDate.addingTimeInterval(TimeInterval(24 * 3600 * index))
            let dateStr = Constants.zoomDateFormatter.string(from: currDate)
            let dateComponents = dateStr.components(separatedBy: " ")
            let folderPath = folder + "/" + dateComponents.first!
            let url = Bundle.main.url(forResource: dateComponents.last!, withExtension: "json", subdirectory: folderPath)!
            let data = try! Data(contentsOf: url)
            let graph = try! Constants.decoder.decode(Graph.self, from: data)
            if index == 0 {
                result = graph
            } else {
                var columns = result.columns
                for (index, column) in graph.columns.enumerated() {
                    columns[index].values += column.values
                }
                result.columns = columns
            }
        }
        
        let windowWidth = CGFloat(1.0) / CGFloat(days)
        let leftOffset = CGFloat(startDate.days(to: date)) / CGFloat(days)
        
        return SimpleGraphViewModel(graph: result, leftOffset: leftOffset, windowWidth:windowWidth, hiddenColumns: self.hiddenColumns)
    }
    
    func windowDidChange() {
        updateMainPlotValues()
    }
    
    func getYRanges(isThumb: Bool) -> [(min: Int, max: Int)] {
        let ranges = isThumb ? thumbYRanges : yRanges
        return Array(repeating: ranges, count: graph.lineColumns.count)
    }
    
    func getHeightScales(isThumb: Bool) -> [CGFloat] {
        return isThumb ? thumbHeightScales : heightScales
    }
    
    func getPoints(isThumb: Bool) ->[[CGPoint]] {
        return isThumb ? thumbPoints : points
    }
    
    private func boundValuesFor(minEl: Int, maxEl: Int) -> (min: Int, max: Int) {
        let diff = maxEl - minEl
        let delta = Int(round(Double(diff) * 0.05))
        let numberOfDigits = max(String(describing: diff).count - 2, 0)
        let multiplier = Int(round(pow(10.0, Double(numberOfDigits))))
        var minYBound = Int(floor(Double(minEl - delta) / Double(multiplier))) * multiplier
        if minYBound < 100 {
            minYBound = 0
        }
        let way = maxEl - minYBound
        let step = Double(way) / Double(linesCount)
        let maxYBound = minYBound + Int(ceil(Double(step / Double(multiplier)))) * multiplier * linesCount
        return (min: minYBound, max: maxYBound)
    }
    
    private func updateYRanges() {
        let elementsCount = graph.xColumn.values.count
        let firstIndex = Int(floor(leftOffset * CGFloat(elementsCount)))
        let lastIndex = min(Int(ceil((leftOffset + windowWidth) * CGFloat(elementsCount))), elementsCount - 1)
        let lineColumns = graph.lineColumns
        let notHiddenValues = notHiddenColumns.flatMap { lineColumns[$0].values[firstIndex...lastIndex] }
        let maxEl = notHiddenValues.max()!
        let minEl = notHiddenValues.min()!
        yRanges = boundValuesFor(minEl: minEl, maxEl: maxEl)
    }
    
    private func updateThumbYRanges() {
        let lineColumns = graph.lineColumns
        let notHiddenThumbValues = notHiddenColumns.flatMap { lineColumns[$0].values }
        thumbYRanges = boundValuesFor(minEl: notHiddenThumbValues.min()!, maxEl: notHiddenThumbValues.max()!)
    }
    
    private func updateDatesRange() {
        let xColumn = graph.xColumn
        startDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.min()!) / 1000.0)
        endDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.max()!) / 1000.0)
    }
    
    private func calculateScales(isThumb: Bool) -> [CGFloat] {
        
        let lineColumns = graph.lineColumns
        
        var scales: [CGFloat] = []
        let ranges = getYRanges(isThumb: isThumb).first!
        
        for column in lineColumns {
            scales.append(CGFloat(column.values.max()! - ranges.min) / CGFloat(ranges.max - ranges.min))
        }
        return scales
    }
    
    private func calculatePoints(isThumb: Bool) -> [[CGPoint]] {
        
        let yColumns = graph.lineColumns
        let xValues = graph.xColumn.values
        
        let maxX = xValues.max()!
        let minX = xValues.min()!
        
        let minY = getYRanges(isThumb: isThumb).first!.min
        
        var points: [[CGPoint]] = Array(repeating: [], count: yColumns.count)
        
        for (i, xValue) in xValues.enumerated() {
            let x = CGFloat(xValue - minX) / CGFloat(maxX - minX)
            for (j, column) in yColumns.enumerated() {
                let maxY = column.values.max()!
                let y = CGFloat(column.values[i] - minY) / CGFloat(maxY - minY)
                let point = CGPoint(x: x, y: y)
                points[j].append(point)
            }
        }
        
        return points
    }
}
