//
//  YScaledGraphViewModel.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 09/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class YScaledGraphViewModel: GraphViewModel {
    
    var graph: Graph
    var hiddenColumns:Set<Int> = []
    var leftOffset: CGFloat = 0.5
    var windowWidth: CGFloat = 0.5
    private let folder: String
    var hourly: Bool = false
    
    private var yRanges: [(min: Int, max: Int)] = [(min: 0, max: 0)]
    private var thumbYRanges: [(min: Int, max: Int)] = [(min: 0, max: 0)]
    
    init(graph: Graph, folder: String) {
        self.graph = graph
        self.folder = folder
        updateDatesRange()
        updateThumbValues()
        updateMainPlotValues()
    }
    
    init(graph: Graph, leftOffset: CGFloat, windowWidth: CGFloat, hiddenColumns: Set<Int>) {
        self.graph = graph
        self.folder = ""
        self.hourly = true
        self.leftOffset = leftOffset
        self.windowWidth = windowWidth
        self.hiddenColumns = hiddenColumns
        updateDatesRange()
        updateThumbValues()
        updateMainPlotValues()
    }
    
    var startDate: Date!
    var endDate: Date!
    
    var coordinateValues: (left: [(color: UIColor, value: Int)]?, right: [(color: UIColor, value: Int)]?) {
        
        var leftValues: [(color: UIColor, value: Int)]? = nil
        let firstColumn = graph.lineColumns.first!
        if !hiddenColumns.contains(0) {
            let step = (yRanges.first!.max - yRanges.first!.min) / linesCount
            leftValues = (0..<linesCount).map {
                return (color: firstColumn.color, value: yRanges.first!.min + step * $0)
            }
        }
        
        var rightValues: [(color: UIColor, value: Int)]? = nil
        let lastColumn = graph.lineColumns.last!
        if !hiddenColumns.contains(1) {
            let step = (yRanges.last!.max - yRanges.last!.min) / linesCount
            rightValues = (0..<linesCount).map {
                return (color: lastColumn.color, value: yRanges.last!.min + step * $0)
            }
        }
        return (left: leftValues, right: rightValues)
    }
    
    private var points: [[CGPoint]] = []
    private var heightScales: [CGFloat] = []
    private var thumbPoints: [[CGPoint]] = []
    private var thumbHeightScales: [CGFloat] = []
    
    private func updateThumbValues() {
        updateThumbYRanges()
        thumbPoints = calculatePoints(isThumb: true)
        thumbHeightScales = calculateScales(isThumb: true)
    }
    
    private func updateMainPlotValues() {
        let prevMinYValues = yRanges.map { $0.min }
        updateYRanges()
        if prevMinYValues != yRanges.map({ $0.min }) || points.count == 0 {
            points = calculatePoints(isThumb: false)
        }
        heightScales = calculateScales(isThumb: false)
    }
    
    func windowDidChange() {
        updateMainPlotValues()
    }
    
    func zoomedViewModel(forDate date: Date) -> GraphViewModel? {
        let calendar = Calendar.current
        var dayIndex = calendar.component(.weekday, from: date) - 2
        if dayIndex < 0 {
            dayIndex = 6
        }
        let startDate = max(self.startDate, date.addingTimeInterval(-TimeInterval(24 * 3600 * dayIndex)))
        let endDate = min(self.endDate, date.addingTimeInterval(TimeInterval(24 * 3600 * (6 - dayIndex))))
        let days = startDate.days(to: endDate) + 1
        
        var result: Graph! = nil
        
        for index in 0..<days {
            let currDate = startDate.addingTimeInterval(TimeInterval(24 * 3600 * index))
            let dateStr = Constants.zoomDateFormatter.string(from: currDate)
            let dateComponents = dateStr.components(separatedBy: " ")
            let folderPath = folder + "/" + dateComponents.first!
            let url = Bundle.main.url(forResource: dateComponents.last!, withExtension: "json", subdirectory: folderPath)!
            let data = try! Data(contentsOf: url)
            let graph = try! Constants.decoder.decode(Graph.self, from: data)
            if index == 0 {
                result = graph
            } else {
                var columns = result.columns
                for (index, column) in graph.columns.enumerated() {
                    columns[index].values += column.values
                }
                result.columns = columns
            }
        }
        
        let windowWidth = CGFloat(1.0) / CGFloat(days)
        let leftOffset = CGFloat(startDate.days(to: date)) / CGFloat(days)
        
        return YScaledGraphViewModel(graph: result, leftOffset: leftOffset, windowWidth:windowWidth, hiddenColumns: self.hiddenColumns)
    }
    
    func getYRanges(isThumb: Bool) -> [(min: Int, max: Int)] {
        return isThumb ? thumbYRanges : yRanges
    }
    
    func getHeightScales(isThumb: Bool) -> [CGFloat] {
        return isThumb ? thumbHeightScales : heightScales
    }
    
    func getPoints(isThumb: Bool) ->[[CGPoint]] {
        return isThumb ? thumbPoints : points
    }
    
    private func boundValuesFor(minEl: Int, maxEl: Int) -> (min: Int, max: Int) {
        let diff = maxEl - minEl
        let delta = Int(round(Double(diff) * 0.05))
        let numberOfDigits = max(String(describing: diff).count - 2, 0)
        let multiplier = Int(round(pow(10.0, Double(numberOfDigits))))
        var minYBound = Int(floor(Double(minEl - delta) / Double(multiplier))) * multiplier
        if minYBound < 100 {
            minYBound = 0
        }
        let way = maxEl - minYBound
        let step = Double(way) / Double(linesCount)
        let maxYBound = minYBound + Int(ceil(Double(step / Double(multiplier)))) * multiplier * linesCount
        return (min: minYBound, max: maxYBound)
    }
    
    private func updateYRanges() {
        let elementsCount = graph.xColumn.values.count
        let firstIndex = Int(floor(leftOffset * CGFloat(elementsCount)))
        let lastIndex = min(Int(ceil((leftOffset + windowWidth) * CGFloat(elementsCount))), elementsCount - 1)
        yRanges = graph.lineColumns.map {
            let values = $0.values[firstIndex...lastIndex]
            return self.boundValuesFor(minEl: values.min()!, maxEl: values.max()!)
        }
    }
    
    private func updateThumbYRanges() {
        thumbYRanges = graph.lineColumns.map {
            let values = $0.values
            return self.boundValuesFor(minEl: values.min()!, maxEl: values.max()!)
        }
    }
    
    private func updateDatesRange() {
        let xColumn = graph.xColumn
        startDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.min()!) / 1000.0)
        endDate = Date(timeIntervalSince1970: TimeInterval(xColumn.values.max()!) / 1000.0)
    }
    
    private func calculateScales(isThumb: Bool) -> [CGFloat] {
        
        let lineColumns = graph.lineColumns
        
        var scales: [CGFloat] = []
        
        for (index, column) in lineColumns.enumerated() {
            let ranges = getYRanges(isThumb: isThumb)[index]
            scales.append(CGFloat(column.values.max()! - ranges.min) / CGFloat(ranges.max - ranges.min))
        }
        return scales
    }
    
    private func calculatePoints(isThumb: Bool) -> [[CGPoint]] {
        
        let yColumns = graph.lineColumns
        let xValues = graph.xColumn.values
        
        let maxX = xValues.max()!
        let minX = xValues.min()!
        
        var points: [[CGPoint]] = Array(repeating: [], count: yColumns.count)
        
        for (i, xValue) in xValues.enumerated() {
            let x = CGFloat(xValue - minX) / CGFloat(maxX - minX)
            for (j, column) in yColumns.enumerated() {
                let ranges = getYRanges(isThumb: isThumb)
                let minY = ranges[j].min
                let maxY = column.values.max()!
                let y = CGFloat(column.values[i] - minY) / CGFloat(maxY - minY)
                let point = CGPoint(x: x, y: y)
                points[j].append(point)
            }
        }
        
        return points
    }
}
