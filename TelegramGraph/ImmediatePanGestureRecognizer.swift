//
//  ImmediatePanGestureRecognizer.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 09/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class ImmediatePanGestureRecognizer: UIPanGestureRecognizer {
    
    private var touchesBegan: Bool = false
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        guard state == .began || state == .changed else { return }
        super.touchesMoved(touches, with: event)
    }
    
    func cancel() {
        touchesBegan = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        guard state == .possible else { return }
        super.touchesBegan(touches, with: event)
        touchesBegan = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if self.state == .possible && self.touchesBegan {
                self.state = .began
            }
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        touchesBegan = false
        super.touchesCancelled(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        touchesBegan = false
        super.touchesEnded(touches, with: event)
    }
}
