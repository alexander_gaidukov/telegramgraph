//
//  TimelineView.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 07/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class TimelineView: UIView {
    private var innerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private let labelWidth: CGFloat = 50.0
    
    var datesInterval: (start: Date, end: Date)?
    var scales: (offset: CGFloat, window: CGFloat)?
    
    private var labels: [UILabel] = []
    
    private var multipliers: [Int] = []
    
    override var bounds: CGRect {
        didSet {
            layoutInnerView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addSubview(innerView)
    }
    
    func configure(model: GraphViewModel) {
        datesInterval = (start: model.startDate, end: model.endDate)
        scales = (offset: model.leftOffset, window: model.windowWidth)
        
        var availableGaps: [Int] = []
        var value = 0
        let daysCount = model.daysCount
        while value < daysCount {
            availableGaps.append(value)
            value = value * 2 + 1
        }
        multipliers = availableGaps.map { $0 + 1 }
        
        labels.forEach { $0.removeFromSuperview() }
        labels = []
        
        for i in 0...daysCount {
            let label = labelForDate(model.dateForTimeline(at: i))
            innerView.addSubview(label)
            labels.append(label)
        }
        
        layoutInnerView()
    }
    
    func updateScales(_ scales: (offset: CGFloat, window: CGFloat)) {
        guard !(self.scales?.offset == scales.offset && self.scales?.window == scales.window) else { return }
        guard scales.window != self.scales?.window else {
            innerView.frame.origin.x = -scales.offset * bounds.width / scales.window
            self.scales = scales
            return
        }
        
        self.scales = scales
        layoutInnerView()
    }
    
    private func findMultiplier() -> Int {
        let preferedItemsCount = Int(floor(innerView.frame.width / labelWidth))
        let calculatedMultiplier = Int(ceil(Double(labels.count) / Double(preferedItemsCount)))
        for multiplier in multipliers {
            if multiplier >= calculatedMultiplier {
                return multiplier
            }
        }
        fatalError()
    }
    
    private func layoutInnerView() {
        guard let scales = scales else { return }
        innerView.frame = CGRect(x: -scales.offset * bounds.width / scales.window, y: 0, width: bounds.width / scales.window, height: bounds.height)
        let steps = labels.count - 1
        let stepWidth = innerView.frame.width / CGFloat(steps)
        let multiplier = findMultiplier()
        for (index, label) in labels.enumerated() {
            label.sizeToFit()
            label.frame = CGRect(x: stepWidth * CGFloat(index) - label.frame.width, y: 0, width: label.frame.width, height: bounds.height)
            label.alpha = (labels.count - 1 - index) % multiplier == 0 ? 1.0 : 0.0
        }
    }
    
    private func labelForDate(_ date: String) -> UILabel {
        let label = UILabel(frame: .zero)
        label.backgroundColor = .clear
        label.textColor = UIColor(hex: "#8E8E93")
        label.font = .systemFont(ofSize: 12)
        label.text = date
        return label
    }
}
