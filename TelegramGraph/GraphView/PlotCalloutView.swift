//
//  PlotCalloutView.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 21/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol PlotCalloutViewDelegate: class {
    func plotCalloutDidSelect(date: Date)
}

class PlotCalloutView: UIView {
    
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var arrowButton: UIButton!
    
    private var dataViews: [UIView] = []
    
    private var bottomConstraint: NSLayoutConstraint!
    
    private var date: Date!
    
    weak var delegate: PlotCalloutViewDelegate?
    
    static func instance() -> PlotCalloutView {
        return Bundle.main.loadNibNamed(String(describing: PlotCalloutView.self), owner: nil, options: nil)?.first as! PlotCalloutView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 4.0
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didSelect))
        addGestureRecognizer(recognizer)
    }
    
    private var theme: Theme!
    
    func applyTheme(_ theme: Theme) {
        self.theme = theme
        backgroundColor = theme.tableViewBackgroundColor
        dateLabel.textColor = theme.titleColor
        arrowButton.tintColor = theme.tooltipArrowColor
        dataViews.forEach { ($0.viewWithTag(1) as! UILabel).textColor = theme.titleColor }
    }
    
    private func addDataView() {
        let view = UIView(frame: .zero)
        view.backgroundColor = .clear
        
        let label = UILabel(frame: .zero)
        label.backgroundColor = .clear
        label.textColor = theme.titleColor
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.tag = 1
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor),
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
        
        let valueLabel = UILabel(frame: .zero)
        valueLabel.backgroundColor = .clear
        valueLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
        valueLabel.textAlignment = .right
        valueLabel.tag = 2
        view.addSubview(valueLabel)
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            valueLabel.topAnchor.constraint(equalTo: view.topAnchor),
            valueLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            valueLabel.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 10.0)
        ])
        
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        let topView: UIView! = dataViews.last ?? dateLabel
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 3.0),
            view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10)
        ])
        
        dataViews.append(view)
    }
    
    func configure(date: Date, values: [PlotLocationValue], hourly: Bool) {
        self.date = date
        let formatter = hourly ? Constants.calloutHourlyDateFormatter : Constants.calloutDateFormatter
        dateLabel.text = formatter.string(from: date)
        let vals = values.filter { $0.value != nil }
        
        if dataViews.count < vals.count {
            if let bottomConstraint = bottomConstraint {
                removeConstraint(bottomConstraint)
                self.bottomConstraint = nil
            }
            (0..<vals.count - dataViews.count).forEach {_ in self.addDataView()}
        } else if dataViews.count > vals.count {
            if let bottomConstraint = bottomConstraint {
                removeConstraint(bottomConstraint)
                self.bottomConstraint = nil
            }
            (0..<dataViews.count - vals.count).forEach {_ in
                dataViews.last!.removeFromSuperview()
                dataViews.removeLast()
            }
        }
        
        if bottomConstraint == nil {
            bottomConstraint = dataViews.last!.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
            bottomConstraint.isActive = true
        }
        
        for (index, dataView) in dataViews.enumerated() {
            let titleLabel = dataView.viewWithTag(1) as! UILabel
            let valueLabel = dataView.viewWithTag(2) as! UILabel
            titleLabel.text = vals[index].name
            valueLabel.textColor = vals[index].color
            valueLabel.text = "\(vals[index].value!.groupingRepresentation)"
        }
    }
    
    @objc private func didSelect() {
        delegate?.plotCalloutDidSelect(date: date)
    }
}
