//
//  SliderView.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 14/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class SliderView: UIView {
    
    private var windowScale: CGFloat = 0.5
    private var leftOffsetScale: CGFloat = 0.5
    
    private let windowMinWidth: CGFloat = 40
    
    private var leftView: UIView!
    private var rightView: UIView!
    private var windowView: SliderWindowView!
    
    var didChanged: ((CGFloat, CGFloat) -> ())?
    var didRelease: (() -> ())?
    
    override var bounds: CGRect {
        didSet {
            updatePositions()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .clear
        
        let leftView = UIView(frame: .zero)
        leftView.backgroundColor = UIColor(hex: "#E2EEF9").withAlphaComponent(0.6)
        leftView.layer.cornerRadius = 4.0
        leftView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        self.leftView = leftView
        
        let rightView = UIView(frame: .zero)
        rightView.backgroundColor = UIColor(hex: "#E2EEF9").withAlphaComponent(0.6)
        rightView.layer.cornerRadius = 4.0
        rightView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        self.rightView = rightView
        
        let windowView = SliderWindowView.instance()
        self.windowView = windowView
        
        addSubview(leftView)
        addSubview(rightView)
        addSubview(windowView)
        
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(windowDidMove(_:)))
        windowView.addGestureRecognizer(panRecognizer)
        
        let leftResizeRecognizer = UIPanGestureRecognizer(target: self, action: #selector(windowDidResizeLeft(_:)))
        windowView.leftSideView.addGestureRecognizer(leftResizeRecognizer)
        
        let rightResizeRecognizer = UIPanGestureRecognizer(target: self, action: #selector(windowDidResizeRight(_:)))
        windowView.rightSideView.addGestureRecognizer(rightResizeRecognizer)
    }
    
    func applyTheme(_ theme: Theme) {
        leftView?.backgroundColor = theme.sliderBackgroundColor
        rightView?.backgroundColor = theme.sliderBackgroundColor
        windowView?.applyTheme(theme)
    }
    
    private func updatePositions() {
        let overlap: CGFloat = 4.0
        var origin: CGFloat = 0.0
        leftView.frame = CGRect(x: origin, y: 0, width: bounds.width * leftOffsetScale + overlap, height: bounds.height)
        origin += leftView.frame.width - overlap
        windowView.frame = CGRect(x: origin, y: 0, width: bounds.width * windowScale, height: bounds.height)
        origin += windowView.frame.width
        rightView.frame = CGRect(x: origin - overlap, y: 0, width: bounds.width - origin + overlap, height: bounds.height)
    }
    
    func setPosition(leftOffset: CGFloat, width: CGFloat) {
        leftOffsetScale = max(min(leftOffset, 1), 0)
        windowScale = max(min(width, 1), 0)
        updatePositions()
    }
    
    private enum SliderRecognizerType {
        case left
        case right
        case move
    }
    
    private func updateWindow(withRecognizer recognizer: UIPanGestureRecognizer, recognizerType type: SliderRecognizerType) {
        guard recognizer.state != .ended else {
            didRelease?()
            return
        }
        
        guard recognizer.state == .changed else { return }
        
        defer {
            recognizer.setTranslation(.zero, in: self)
        }
        
        var translation = recognizer.translation(in: self).x
        var windowFrame = windowView.frame
        
        switch type {
        case .left:
            translation = min(max(translation, -windowFrame.origin.x), windowFrame.width - windowMinWidth)
            windowFrame.size.width -= translation
            windowFrame.origin.x += translation
        case .right:
            translation = min(max(translation, -(windowFrame.width - windowMinWidth)), bounds.width - windowFrame.maxX)
            windowFrame.size.width += translation
        case .move:
            translation = min(max(translation, -windowFrame.origin.x), bounds.width - windowFrame.maxX)
            windowFrame.origin.x += translation
        }
        
        windowScale = windowFrame.size.width / bounds.width
        leftOffsetScale = windowFrame.origin.x / bounds.width
        
        updatePositions()
        didChanged?(windowScale, leftOffsetScale)
    }
    
    @objc private func windowDidMove(_ recognizer: UIPanGestureRecognizer) {
        updateWindow(withRecognizer: recognizer, recognizerType: .move)
    }
    
    @objc private func windowDidResizeLeft(_ recognizer: UIPanGestureRecognizer) {
        updateWindow(withRecognizer: recognizer, recognizerType: .left)
    }
    
    @objc private func windowDidResizeRight(_ recognizer: UIPanGestureRecognizer) {
       updateWindow(withRecognizer: recognizer, recognizerType: .right)
    }
}
