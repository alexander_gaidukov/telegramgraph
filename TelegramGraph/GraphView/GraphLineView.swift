//
//  GraphLineView.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 16/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class GraphLineView: UIView {
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var rightLabel: UILabel!
    @IBOutlet private weak var separatorView: UIView!
    
    static func instance() -> GraphLineView {
        return Bundle.main.loadNibNamed(String(describing: GraphLineView.self), owner: nil, options: nil)?.first as! GraphLineView
    }
    
    func applyTheme(_ theme: Theme) {
        separatorView.backgroundColor = theme.separatorColor
    }
    
    func configure(left: (color: UIColor, value: Int)?, right: (color: UIColor, value: Int)?) {
        label.text = left?.value.stringRepresentation
        rightLabel.text = right?.value.stringRepresentation
        label.textColor = left?.color
        rightLabel.textColor = right?.color
    }
}
