//
//  ColumnCell.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 08/04/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class ColumnCell: UICollectionViewCell {
    
    @IBOutlet private weak var coloredView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var checkmarkImageView: UIImageView!
    @IBOutlet private weak var leftOffsetConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 6.0
        layer.borderWidth = 1
    }
    
    func configure(column: Column, hidden: Bool) {
        titleLabel.text = column.name
        titleLabel.textColor = hidden ? column.color : .white
        coloredView.backgroundColor = hidden ? .clear : column.color
        layer.borderColor = hidden ? column.color.cgColor : UIColor.clear.cgColor
        checkmarkImageView.isHidden = hidden
        leftOffsetConstraint.priority = hidden ? .defaultLow : .defaultHigh
    }

}
