//
//  SlideWindowView.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 14/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class SliderWindowView: UIView {
    
    @IBOutlet weak var leftSideView: UIView!
    @IBOutlet weak var rightSideView: UIView!
    
    static func instance() -> SliderWindowView {
        return Bundle.main.loadNibNamed(String(describing: SliderWindowView.self), owner: nil, options: nil)?.first as! SliderWindowView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 4
        layer.borderWidth = 2
    }
    
    func applyTheme(_ theme: Theme) {
        leftSideView.backgroundColor = theme.sliderWindowColor
        rightSideView.backgroundColor = theme.sliderWindowColor
        layer.borderColor = theme.sliderWindowColor.cgColor
    }
}
