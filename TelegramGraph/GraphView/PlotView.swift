//
//  GraphView.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 13/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit
typealias PlotLocationValue = (name: String, color: UIColor, value: Int?)
typealias PlotLocation = (point: CGPoint, step: CGFloat, date: Date, values: [PlotLocationValue])

protocol PlotViewDelegate: class {
    func plotViewDidStartVerticalLineMove(_ plotView: PlotView)
    func plotViewDidFinishVerticalLineMove(_ plotView: PlotView)
    func plotViewDidClickCallout(date: Date)
}

final class PlotView: UIView {
    private var graphModel: GraphViewModel? = nil
    
    var theme: Theme! {
        didSet {
            calloutView.applyTheme(theme)
            circleLayers.forEach {
                $0.fillColor = theme.cellBackgroundColor.cgColor
            }
            whiteMaskLayer?.fillColor = theme.lightenMask.cgColor
            verticalLine.backgroundColor = theme.separatorColor
        }
    }
    
    weak var delegate: PlotViewDelegate?
    
    var thumbnail: Bool = false
    
    private var layers: [CAShapeLayer] = []
    private var textLayers: [CATextLayer] = []
    private var initialPaths: [CGPath] = []
    private var angles: [CGFloat] = []
    private var whiteMaskLayer: CAShapeLayer?
    
    private let verticalLine = UIView(frame: .zero)
    
    func setModel(graphModel: GraphViewModel, animated: Bool) {
        self.graphModel = graphModel
        configureLayers(animated: animated)
    }

    private lazy var calloutView: PlotCalloutView = {
        let calloutView = PlotCalloutView.instance()
        calloutView.delegate = self
        return calloutView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        transform = CGAffineTransform(scaleX: 1, y: -1)
        clipsToBounds = true
        updateVerticalLine()
    }
    
    private func updateVerticalLine() {
        guard !thumbnail else { return }
        verticalLine.frame = CGRect(x: 0, y: 0, width: 1.0, height: bounds.height - 5.0)
    }
    
    override var bounds: CGRect {
        didSet {
            layoutLayers()
            updateVerticalLine()
        }
    }
    
    private func layoutLayers(animated: Bool = false) {
        layers.forEach {
            $0.frame = self.bounds
        }
        
        let radius = min(bounds.width, bounds.height) / 2.0 - 25
        textLayers.forEach {
            $0.frame = CGRect(x: bounds.midX - 16, y: bounds.midY + radius, width: 32, height: 16)
        }
        
        updateInitilaPath()
        if animated {
            animatePathsUpdate()
        } else {
            updatePaths()
        }
    }
    
    private func calculateAngles() {
        angles = []
        var firstAngle: CGFloat = 0
        var previousAngle: CGFloat = 0
        for (index, scale) in graphModel!.getHeightScales(isThumb: false).enumerated() {
            let angle = -2 * CGFloat.pi * scale
            if index == 0 {
                angles.append(0)
                firstAngle = angle / 2.0
            } else {
                angles.append(previousAngle + firstAngle + angle / 2.0)
                previousAngle += angle
            }
        }
    }
    
    private func updatePaths() {
        for (index, layer) in layers.enumerated() {
            layer.path = pathForLayer(at: index)
            layer.opacity = graphModel!.hiddenColumns.contains(index) ? 0.0 : 1.0
            if !angles.isEmpty {
                layer.transform = CATransform3DRotate(CATransform3DIdentity, angles[index], 0, 0, 1)
            } else {
                layer.transform = CATransform3DIdentity
            }
        }
    }
    
    func updateColumns() {
        updateInitilaPath()
        animatePathsUpdate()
    }
    
    func windowDidChange() {
        if !(graphModel is PieChartGraphModel) {
            updatePaths()
        }
    }
    
    private func updateInitilaPath() {
        initialPaths.removeAll()
        initialPaths = (0..<layers.count).map(initialPathForLayer)
        if graphModel is PieChartGraphModel, !thumbnail {
            calculateAngles()
        } else {
            angles = []
        }
        let scales = graphModel!.getHeightScales(isThumb: false)
        for (index, textLayer) in textLayers.enumerated() {
            textLayer.string = scales[index] > CGFloat.ulpOfOne ?  "\(Int(round(scales[index] * 100)))%" : nil
        }
    }
    
    private func animatePathsUpdate() {
        for (index, layer) in layers.enumerated() {

            let path = pathForLayer(at: index)
            let isHidden = graphModel!.hiddenColumns.contains(index)

            CATransaction.begin()
            CATransaction.setAnimationDuration(Constants.animationDuration)

            let pathAnimation = CABasicAnimation(keyPath: "path")
            pathAnimation.fromValue = layer.path
            pathAnimation.toValue = path
            layer.path = path


            let opacity: Float = isHidden ? 0 : 1
            let opacityAnimation = CABasicAnimation(keyPath: "opacity")
            opacityAnimation.fromValue = layer.opacity
            opacityAnimation.toValue = opacity
            layer.opacity = opacity
            
            var animations = [pathAnimation, opacityAnimation]
            let startValue = layer.value(forKeyPath: "transform.rotation.z") as? CGFloat
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
            rotationAnimation.fromValue = startValue
            let toValue = angles.isEmpty ? 0 : angles[index]
            rotationAnimation.toValue = toValue
            layer.transform = CATransform3DRotate(CATransform3DIdentity, toValue , 0, 0, 1)
            animations.append(rotationAnimation)

            let anims = CAAnimationGroup()
            anims.animations = [pathAnimation, opacityAnimation]
            anims.isRemovedOnCompletion = true
            anims.fillMode = .forwards
            
            layer.add(anims, forKey: nil)

            CATransaction.commit()
        }
    }
    
    private func pathForLayer(at index: Int) -> CGPath {
        guard !(graphModel is PieChartGraphModel) else { return initialPaths[index] }
        let yScale = graphModel!.getHeightScales(isThumb: thumbnail)[index]
        let xScale = thumbnail ? 1 : 1 / graphModel!.windowWidth
        var path = initialPaths[index].scale(by: CGPoint(x: xScale, y: yScale))
        
        if !thumbnail {
            path = path.translate(by: CGPoint(x: -bounds.width * xScale * graphModel!.leftOffset, y: 0))
        }
        return path
    }
    
    private func initialPathForPieChart(at index: Int) -> CGPath {
        let scales = graphModel!.getHeightScales(isThumb: false)
        let scale = scales[index]
        let path = UIBezierPath()
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let radius = min(bounds.width, bounds.height) / 2.0
        let angle = 2.0 * CGFloat.pi * scale
//        let previousAngle = scales[0..<index].reduce(0, +) * 2.0 * CGFloat.pi
//        let startAngle = CGFloat.pi / 2 - previousAngle
//        let endAngle = startAngle - angle
//        let startPoint = CGPoint(x: center.x + cos(startAngle) * radius, y: center.y + sin(startAngle) * radius)
//        let endPoint = CGPoint(x: center.x + cos(endAngle) * radius, y: center.y + sin(endAngle) * radius)
//        path.move(to: center)
//        path.addLine(to: startPoint)
//        path.addArc(withCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)
//        path.addLine(to: endPoint)
        let deltaX = radius * sin(angle / 2.0)
        let deltaY = radius * cos(angle / 2.0)
        path.move(to: center)
        path.addLine(to: CGPoint(x: center.x - deltaX, y: center.y + deltaY))
        path.addArc(withCenter: center, radius: radius, startAngle: CGFloat.pi / 2 + angle / 2, endAngle: CGFloat.pi / 2 - angle / 2, clockwise: false)
        path.close()
        return path.cgPath
    }
    
    private func initialPathForLayer(at index: Int) -> CGPath {
        
        if graphModel is PieChartGraphModel, !thumbnail {
            return initialPathForPieChart(at: index)
        }
        
        let isBar = graphModel!.graph.isBar
        let isFill = isBar || graphModel!.graph.isArea
        let path = UIBezierPath()
        let pts = graphModel!.getPoints(isThumb: thumbnail)[index]
        var prevY: CGFloat = 0
        for (i, point) in pts.enumerated() {
            let p = CGPoint(x: point.x * bounds.width, y: point.y * bounds.height)
            defer {
                prevY = p.y
            }
            if i == 0 {
                path.move(to: p)
                continue
            }
            if isBar {
                path.addLine(to: CGPoint(x: p.x, y: prevY))
                path.addLine(to: p)
            } else {
                path.addLine(to: p)
            }
        }
        
        if isBar {
            path.addLine(to: CGPoint(x: bounds.width, y: prevY))
        }
        
        if isFill {
            path.addLine(to: CGPoint(x: bounds.width, y: 0))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.close()
        }
        
        return path.cgPath
    }
    
    private func configureLayers(animated: Bool) {
        guard let graphModel = graphModel else { return }
        let isFill = graphModel.graph.isBar || graphModel.graph.isArea
        let graph = graphModel.graph
        let yColumns = graph.lineColumns
        
        if layers.count > yColumns.count {
            for _ in 0..<(layers.count - yColumns.count) {
                let layer = layers.remove(at: 0)
                layer.removeFromSuperlayer()
            }
        }
        
        for (index, layer) in layers.enumerated() {
            layer.fillColor = isFill ? yColumns[index].color.cgColor : nil
            layer.strokeColor = isFill ? nil : yColumns[index].color.cgColor
        }
        
        if yColumns.count > layers.count {
            for i in layers.count..<yColumns.count {
                let layer = CAShapeLayer()
                layer.fillColor = isFill ? yColumns[i].color.cgColor : nil
                layer.strokeColor = isFill ? nil : yColumns[i].color.cgColor
                layer.lineWidth = 1
                layers.append(layer)
                self.layer.insertSublayer(layer, at: 0)
            }
        }
        
        layers.forEach {
            $0.anchorPoint = (graphModel is PieChartGraphModel) ? CGPoint(x: 0.5, y: 0.5) : .zero
        }
        
        
        if graphModel is PieChartGraphModel, !thumbnail {
            layers.forEach {
                let textLayer = CATextLayer()
                textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 1).fontName as CFString, 2.0, nil)
                textLayer.fontSize = 12.0
                textLayer.foregroundColor = UIColor.white.cgColor
                textLayer.isWrapped = true
                textLayer.alignmentMode = .center
                textLayer.contentsScale = UIScreen.main.scale
                textLayer.string = "Hello"
                textLayer.setAffineTransform(CGAffineTransform(scaleX: 1, y: -1))
                $0.addSublayer(textLayer)
                self.textLayers.append(textLayer)
            }
        } else {
            textLayers.forEach { $0.removeFromSuperlayer() }
            textLayers = []
        }
        
        layoutLayers(animated: animated)
    }
    
    private func plotPoint(for point: CGPoint) -> PlotLocation {
        let daysCount = graphModel!.graph.xColumn.values.count - 1
        let pathWidth = bounds.width / graphModel!.windowWidth
        let step = pathWidth / CGFloat(daysCount)
        let pathOffset = pathWidth * graphModel!.leftOffset
        var newPoint = CGPoint(x: point.x + pathOffset, y: point.y)
        let index = min(max(Int(round(newPoint.x / step)), 0), daysCount)
        newPoint.x = CGFloat(index) * step - pathOffset
        return (point: newPoint, step: step, date: graphModel!.date(at: index), values: graphModel!.plotLocationValues(at: index))
    }
    
    private func configureCallout(location: PlotLocation, began: Bool) {
        if !graphModel!.graph.isBar {
            verticalLine.frame.origin.x = location.point.x - verticalLine.frame.size.width / 2.0
            circleLayers.forEach { $0.removeFromSuperlayer() }
            circleLayers = []
            for (index, params) in location.values.enumerated() {
                guard let value = params.value else { continue }
                let ranges = self.graphModel!.getYRanges(isThumb: false)[index]
                let height = self.bounds.height * CGFloat(value - ranges.min) / CGFloat(ranges.max - ranges.min)
                self.addCircleLayer(withCenter: CGPoint(x: location.point.x, y: height), color: params.color)
            }
        } else {
            let windowPath = UIBezierPath(rect: CGRect(x: location.point.x, y: 0, width: location.step, height: bounds.height))
            let path = UIBezierPath(cgPath: pathForLayer(at: layers.count - 1))
            path.append(windowPath)
            whiteMaskLayer?.path = path.cgPath
        }
        
        calloutView.configure(date: location.date, values: location.values, hourly: graphModel!.hourly)
        var frame = calloutView.frame
        if began {
            calloutView.setNeedsLayout()
            calloutView.layoutIfNeeded()
            var size = calloutView.systemLayoutSizeFitting(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
            size.width = size.width * 1.1
            frame.size = size
            frame.origin.y = self.frame.origin.y
        }
        
        let calloutOffset: CGFloat = 5.0
        frame.origin.x = self.frame.origin.x + location.point.x + calloutOffset
        if frame.maxX > self.frame.maxX {
            frame.origin.x = self.frame.origin.x + location.point.x - frame.size.width - calloutOffset
        }
        calloutView.frame = frame
    }
    
    private var circleLayers: [CAShapeLayer] = []
    
    private func addCircleLayer(withCenter center: CGPoint, color: UIColor) {
        let layer = CAShapeLayer()
        layer.frame = self.layer.bounds
        layer.fillColor = theme.cellBackgroundColor.cgColor
        layer.strokeColor = color.cgColor
        layer.lineWidth = 2.0
        let radius: CGFloat = 2.0
        let path = UIBezierPath(roundedRect: CGRect(x: center.x - radius, y: center.y - radius, width: radius * 2.0, height: radius * 2.0), cornerRadius: radius)
        layer.path = path.cgPath
        self.layer.addSublayer(layer)
        circleLayers.append(layer)
        
    }
    
    @objc func didPan(_ recognizer: UIPanGestureRecognizer) {
        guard !(graphModel is PieChartGraphModel) else { return }
        switch recognizer.state {
        case .began:
            touchesBegan(recognizer.location(in: self))
        case .changed:
            touchesMoved(recognizer.location(in: self))
        case .cancelled, .failed, .ended:
            touchesEnded()
        default:
            break
        }
    }
    
    private func addWhiteMaskLayer() {
        let layer = CAShapeLayer()
        layer.fillColor = theme.lightenMask.cgColor
        layer.strokeColor = nil
        layer.anchorPoint = CGPoint(x: 0, y: 0)
        layer.frame = self.layer.bounds
        self.layer.addSublayer(layer)
        whiteMaskLayer = layer
    }
    
    private func touchesBegan(_ point: CGPoint) {
        guard !thumbnail else { return }
        removeCallout()
        delegate?.plotViewDidStartVerticalLineMove(self)
        let location = plotPoint(for: point)
        if !graphModel!.graph.isBar {
            addSubview(verticalLine)
        } else {
            addWhiteMaskLayer()
        }
        configureCallout(location: location, began: true)
        superview?.addSubview(calloutView)
    }

    private func touchesEnded() {
        guard !thumbnail else { return }
        delegate?.plotViewDidFinishVerticalLineMove(self)
    }

    private func touchesMoved(_ point: CGPoint) {
        guard !thumbnail else { return }
        let location = plotPoint(for: point)
        configureCallout(location: location, began: false)
    }
    
    func removeCallout() {
        calloutView.removeFromSuperview()
        if !graphModel!.graph.isBar {
            verticalLine.removeFromSuperview()
            circleLayers.forEach { $0.removeFromSuperlayer() }
            circleLayers = []
        } else {
            whiteMaskLayer?.removeFromSuperlayer()
            whiteMaskLayer = nil
        }
    }
}

extension PlotView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension PlotView: PlotCalloutViewDelegate {
    func plotCalloutDidSelect(date: Date) {
        removeCallout()
        delegate?.plotViewDidClickCallout(date: date)
    }
}
