//
//  CoordinatesView.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 16/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

typealias CoordinateValues = (left: [(color: UIColor, value: Int)]?, right: [(color: UIColor, value: Int)]?)

func ==(lhs: CoordinateValues, rhs: CoordinateValues) -> Bool {
    return lhs.left?.map({ $0.value }) == rhs.left?.map({ $0.value }) && lhs.right?.map({ $0.value }) == rhs.right?.map({ $0.value })
}

class CoordinatesView: UIView {
    
    private enum CoordinatesAnimationDirection {
        case up
        case down
    }
    
    private var lineViews: [GraphLineView] = []
    
    private var coordinateValues: CoordinateValues = (left: nil, right: nil)
    
    private var theme: Theme!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lineViews = (0..<5).map {_ in GraphLineView.instance() }
        lineViews.forEach { self.addSubview($0) }
        layoutLines()
    }
    
    override var bounds: CGRect {
        didSet {
            layoutLines()
        }
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return false
    }
    
    func applyTheme(_ theme: Theme) {
        self.theme = theme
        lineViews.forEach { $0.applyTheme(theme) }
    }
    
    func layoutLines() {
        let height = bounds.height / CGFloat(lineViews.count)
        for i in 0..<lineViews.count {
            let x: CGFloat = 0.0
            let width = bounds.width
            let y = bounds.height - CGFloat(i + 1) * height
            lineViews[i].frame = CGRect(x: x, y: y, width: width, height: height)
        }
    }
    
    func setCoordinateValues(_ coordinateValues: CoordinateValues, animated: Bool) {
        guard !(coordinateValues == self.coordinateValues) else { return }
        
        guard animated else {
            for i in 0..<lineViews.count {
                lineViews[i].configure(left: coordinateValues.left?[i], right: coordinateValues.right?[i])
            }
            self.coordinateValues = coordinateValues
            return
        }
        
        var leftMoveDown: Bool = false
        var leftMoveUp: Bool = false
        if let leftValues = coordinateValues.left, let oldLeftValues = self.coordinateValues.left {
            if leftValues.last!.value > oldLeftValues.last!.value || leftValues.first!.value > oldLeftValues.first!.value {
                leftMoveDown = true
            } else if leftValues.last!.value < oldLeftValues.last!.value || leftValues.first!.value < oldLeftValues.first!.value {
                leftMoveUp = true
            }
        }
        
        var rightMoveDown: Bool = false
        var rightMoveUp: Bool = false
        if let rightValues = coordinateValues.right, let oldRightValues = self.coordinateValues.right {
            if rightValues.last!.value > oldRightValues.last!.value || rightValues.first!.value > oldRightValues.first!.value {
                rightMoveDown = true
            } else if rightValues.last!.value < oldRightValues.last!.value || rightValues.first!.value < oldRightValues.first!.value {
                rightMoveUp = true
            }
        }
        
        guard leftMoveDown || rightMoveDown || leftMoveUp || rightMoveUp else {
            for i in 0..<lineViews.count {
                lineViews[i].configure(left: coordinateValues.left?[i], right: coordinateValues.right?[i])
            }
            self.coordinateValues = coordinateValues
            return
        }
        
        let direction: CoordinatesAnimationDirection = (leftMoveDown || rightMoveDown) ? .down : .up
        self.coordinateValues = coordinateValues
        self.animateCoordinates(direction: direction)
    }
    
    private func animateCoordinates(direction: CoordinatesAnimationDirection) {
        let additionalLineView = GraphLineView.instance()
        additionalLineView.applyTheme(theme)
        let lastLineView = lineViews.last!
        var extendedLinesArray = lineViews
        let startIndex: Int
        let endIndex: Int
        if direction == .up {
            additionalLineView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: lastLineView.frame.height)
            extendedLinesArray.insert(additionalLineView, at: 0)
            startIndex = 0
            endIndex = extendedLinesArray.count - 1
        } else {
            additionalLineView.frame = CGRect(x: 0, y: -lastLineView.frame.height, width: bounds.width, height: lastLineView.frame.height)
            extendedLinesArray.append(additionalLineView)
            startIndex = 1
            endIndex = extendedLinesArray.count
        }
        addSubview(additionalLineView)
        let move = direction == .up ? -lastLineView.frame.height : lastLineView.frame.height
        UIView.animate(withDuration: Constants.animationDuration, animations: {
            for i in 0..<extendedLinesArray.count {
                extendedLinesArray[i].frame.origin.y += move
                if i >= startIndex && i < endIndex {
                    extendedLinesArray[i].configure(left: self.coordinateValues.left?[i - startIndex], right: self.coordinateValues.right?[i - startIndex])
                }
                
            }
        }, completion: {_ in
            let indexToRemove = direction == .up ? 5 : 0
            extendedLinesArray.remove(at: indexToRemove)
            self.lineViews = extendedLinesArray
        })
    }
}
