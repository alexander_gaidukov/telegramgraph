//
//  ViewController.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 13/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var modeBarButtonItem: UIBarButtonItem!
    
    var isScrolling: Bool = false
    
    private var graphs: [GraphViewModel] = [] {
        didSet {
            cells = graphs.map {
                let cell = Bundle.main.loadNibNamed(String(describing: GraphCell.self), owner: nil, options: nil)!.first as! GraphCell
                cell.delegate = self
                cell.configure(model: $0, theme: Constants.theme)
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
                return cell
            }
        }
    }
    
    private var cells: [GraphCell] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        loadData()
    }
    
    private func loadData() {
        
        self.graphs = (1...5).map {
            let url = Bundle.main.url(forResource: "chart_data_\($0)", withExtension: "json")!
            let data = try! Data(contentsOf: url)
            let graph = try! Constants.decoder.decode(Graph.self, from: data)
            if graph.yScaled {
                return YScaledGraphViewModel(graph: graph, folder: "\($0)")
            }
            if graph.percentage {
                return PercentageAreaGraphModel(graph: graph, folder: "\($0)")
            }
            if graph.stacked {
                return StackedBarGraphViewModel(graph: graph, folder: "\($0)")
            }
            return SimpleGraphViewModel(graph:graph, folder: "\($0)")
        }
    }
    
    private func applyTheme() {
        navigationController?.navigationBar.barTintColor = Constants.theme.navbarBackgroundColor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: Constants.theme.titleColor]
        tableView.backgroundColor = Constants.theme.tableViewBackgroundColor
        cells.forEach {
            $0.applyTheme(Constants.theme)
        }
        modeBarButtonItem.title = Constants.theme.switchText
    }
    
    @IBAction private func switchMode() {
        Constants.theme.toggle()
        applyTheme()
    }
}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return graphs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cells[indexPath.section]
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Followers"
        case 1:
            return "Interactions"
        case 2:
            return "Messages"
        case 3:
            return "Views"
        case 4:
            return "Apps"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cells[indexPath.section].height(forWidth: tableView.bounds.width)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            isScrolling = false
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isScrolling = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isScrolling {
            cells.forEach { $0.cancelGestureRecognizer() }
            isScrolling = true
        }
        
    }
}

extension ViewController: GraphCellDelegate {
    func shouldEnableScrolling() {
        tableView.isScrollEnabled = true
    }
    
    func shouldDisableScrollling() {
        tableView.isScrollEnabled = false
    }
}

