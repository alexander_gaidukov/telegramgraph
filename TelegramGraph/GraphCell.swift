//
//  GraphCell.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 18/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol GraphCellDelegate: class {
    func shouldDisableScrollling()
    func shouldEnableScrolling()
}

class GraphCell: UITableViewCell {
    
    @IBOutlet private weak var plotView: PlotView!
    @IBOutlet private weak var thumbnailPlotView: PlotView!
    @IBOutlet private weak var coordinatesView: CoordinatesView!
    @IBOutlet private weak var timelineView: TimelineView!
    @IBOutlet private weak var sliderView: SliderView!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var dateRangesLabel: UILabel!
    @IBOutlet private weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var zoomOutButton: UIButton!
    @IBOutlet private weak var thumbOffsetConstraint: NSLayoutConstraint!
    @IBOutlet private weak var thumbContainerView: UIView!
    
    weak var delegate: GraphCellDelegate?
    
    private let cellHeight: CGFloat = 50.0
    private var lineColumns: [Column] = []
    
    private var theme: Theme = .day
    private var model: GraphViewModel!
    
    private var originalModel: GraphViewModel!
    
    private var panGestureRecognizer: ImmediatePanGestureRecognizer!
    
    deinit {
        collectionView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    func cancelGestureRecognizer() {
        panGestureRecognizer.cancel()
    }
    
    func height(forWidth width: CGFloat) -> CGFloat {
        var height = 153.0 + (width - 20.0) * 2.0 / 3.0 + collectionViewHeightConstraint.constant
        if collectionViewHeightConstraint.constant < .ulpOfOne {
            height -= 15.0
        }
        if !model.hasThumb {
            height -= 65.0
        }
        return height
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        thumbnailPlotView.layer.cornerRadius = 4.0
        collectionView.register(cell: ColumnCell.self)
        thumbnailPlotView.thumbnail = true
        sliderView.didChanged = {[unowned self] windowScale, leftOffset in
            if self.model is PieChartGraphModel {
                self.originalModel?.leftOffset = leftOffset
                self.originalModel.windowWidth = windowScale
            }
            self.plotView.removeCallout()
            self.model?.leftOffset = leftOffset
            self.model?.windowWidth = windowScale
            self.plotView.windowDidChange()
            self.timelineView.updateScales((offset: leftOffset, window: windowScale))
            self.updateDateRangesLabel()
        }
        
        sliderView.didRelease = {[unowned self] in
            guard !(self.model is PercentageAreaGraphModel) else {
                return
            }
            DispatchQueue.global().async {
                self.model.windowDidChange()
                DispatchQueue.main.async {
                    self.updatePlots()
                }
            }
        }
        
        
        
        let recognizer = ImmediatePanGestureRecognizer(target: plotView, action: #selector(PlotView.didPan(_:)))
        recognizer.delegate = plotView
        plotView.addGestureRecognizer(recognizer)
        panGestureRecognizer = recognizer
        
        collectionView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    private func updatePlots() {
        if !coordinatesView.isHidden {
            coordinatesView.setCoordinateValues(model.coordinateValues, animated: true)
        }
        plotView.updateColumns()
        thumbnailPlotView.updateColumns()
    }
    
    func configure(model: GraphViewModel, theme: Theme) {
        applyTheme(theme)
        updateModel(model: model, animated: false)
        plotView.delegate = self
    }
    
    private func updateModel(model: GraphViewModel, animated: Bool) {
        self.model = model
        plotView.setModel(graphModel: model, animated: animated)
        thumbnailPlotView.setModel(graphModel: model, animated: animated)
        coordinatesView.isHidden = model is PieChartGraphModel
        if !coordinatesView.isHidden {
            coordinatesView.setCoordinateValues(model.coordinateValues, animated: animated)
        }
        timelineView.isHidden = model is PieChartGraphModel
        timelineView.configure(model: model)
        sliderView.setPosition(leftOffset: model.leftOffset, width: model.windowWidth)
        lineColumns = model.graph.lineColumns
        updateDateRangesLabel()
        if lineColumns.count > 1 {
            collectionView.isHidden = false
            collectionView.reloadData()
        } else {
            collectionView.isHidden = true
            collectionViewHeightConstraint.constant = 0.0
            collectionView.reloadData()
        }
        if model.hasThumb {
            thumbContainerView.isHidden = false
            thumbOffsetConstraint.priority = .defaultHigh
        } else {
            thumbContainerView.isHidden = true
            thumbOffsetConstraint.priority = .defaultLow
        }
    }
    
    func updateDateRangesLabel() {
        let ranges = model.displayedDateRanges
        let start = Constants.dateRangesDateFormatter.string(from: ranges.start)
        let end = Constants.dateRangesDateFormatter.string(from: ranges.end)
        if start == end {
            dateRangesLabel.text = "\(start)"
        } else {
            dateRangesLabel.text = "\(start) - \(end)"
        }
    }
    
    func applyTheme(_ theme: Theme) {
        self.theme = theme
        self.contentView.backgroundColor = theme.cellBackgroundColor
        dateRangesLabel.textColor = theme.titleColor
        coordinatesView.applyTheme(theme)
        plotView.theme = theme
        sliderView.applyTheme(theme)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let height = collectionView.contentSize.height
        if height != collectionViewHeightConstraint.constant && lineColumns.count > 1 {
            collectionViewHeightConstraint.constant = collectionView.contentSize.height
        }
    }
    
    @IBAction private func zoomOut() {
        guard let originalModel = originalModel else { return }
        plotView.removeCallout()
        updateModel(model: originalModel, animated: true)
        self.originalModel = nil
        self.zoomOutButton.isHidden = true
        self.dateRangesLabel.textAlignment = .center
    }
}

extension GraphCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lineColumns.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(cell: ColumnCell.self, at: indexPath)
        let column = lineColumns[indexPath.row]
        cell.configure(column: column, hidden: model.hiddenColumns.contains(indexPath.row))
        return cell
    }
}

extension GraphCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let show = model.hiddenColumns.contains(indexPath.row)
        guard show || model.canHideColumns else { return }
        plotView.removeCallout()
        DispatchQueue.global().async {
            if show {
                self.model.hiddenColumns.remove(indexPath.row)
                self.originalModel?.hiddenColumns.remove(indexPath.row)
            } else {
                self.model.hiddenColumns.insert(indexPath.row)
                if self.originalModel?.canHideColumns == true {
                    self.originalModel?.hiddenColumns.insert(indexPath.row)
                }
            }
            
            DispatchQueue.main.async {
                self.updatePlots()
                self.collectionView.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 34.0
        let column = lineColumns[indexPath.row]
        let width = (column.name as NSString).boundingRect(with: CGSize(width: .greatestFiniteMagnitude, height: 18.0), options: .usesLineFragmentOrigin, attributes: [.font: UIFont.systemFont(ofSize: 15.0)], context: nil).size.width + 43.0
        return CGSize(width: width, height: height)
    }
}

extension GraphCell: PlotViewDelegate {
    func plotViewDidStartVerticalLineMove(_ plotView: PlotView) {
        delegate?.shouldDisableScrollling()
    }
    
    func plotViewDidFinishVerticalLineMove(_ plotView: PlotView) {
        delegate?.shouldEnableScrolling()
    }
    
    func plotViewDidClickCallout(date: Date) {
        guard originalModel == nil else { return }
        guard let model = self.model.zoomedViewModel(forDate: date) else { return }
        originalModel = self.model
        updateModel(model: model, animated: true)
        self.zoomOutButton.isHidden = false
        self.dateRangesLabel.textAlignment = .right
    }
}
