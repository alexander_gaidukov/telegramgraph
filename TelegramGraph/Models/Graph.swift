//
//  Graph.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 13/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

fileprivate final class Color: UIColor, Decodable {
    convenience init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.init(hex: try container.decode(String.self))
    }
}

struct Graph: Decodable {
    
    var columns: [Column]
    let yScaled: Bool
    let stacked: Bool
    let percentage: Bool
    
    var xColumn: Column {
        return columns.filter { $0.type == .x }.first!
    }
    
    var lineColumns: [Column] {
        return columns.filter { $0.type != .x }
    }
    
    var isBar: Bool {
        return lineColumns.first!.type == .bar
    }
    
    var isArea: Bool {
        return lineColumns.first!.type == .area
    }
    
    private enum CodingKeys: String, CodingKey {
        case columns
        case types
        case names
        case colors
        case yScaled
        case stacked
        case percentage
    }
    
    private struct DynamicKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        var intValue: Int?
        init?(intValue: Int) {
            return nil
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.yScaled = (try? container.decode(Bool.self, forKey: .yScaled)) ?? false
        self.stacked = (try? container.decode(Bool.self, forKey: .stacked)) ?? false
        self.percentage = (try? container.decode(Bool.self, forKey: .percentage)) ?? false
        var columns = try container.decode([Column].self, forKey: .columns)
        
        let typesContainer = try container.nestedContainer(keyedBy: DynamicKeys.self, forKey: .types)
        var types: [String: ColumnType] = [:]
        for key in typesContainer.allKeys {
            types[key.stringValue] = try typesContainer.decode(ColumnType.self, forKey: key)
        }
        
        let namesContainer = try container.nestedContainer(keyedBy: DynamicKeys.self, forKey: .names)
        var names: [String: String] = [:]
        for key in namesContainer.allKeys {
            names[key.stringValue] = try namesContainer.decode(String.self, forKey: key)
        }
        
        let colorsContainer = try container.nestedContainer(keyedBy: DynamicKeys.self, forKey: .colors)
        var colors: [String: UIColor] = [:]
        for key in colorsContainer.allKeys {
            colors[key.stringValue] = try colorsContainer.decode(Color.self, forKey: key)
        }
        
        for (index, column) in columns.enumerated() {
            var c = column
            c.type = types[c.label]
            c.name = names[c.label]
            c.color = colors[c.label]
            columns[index] = c
        }
        
        self.columns = columns
    }
}
