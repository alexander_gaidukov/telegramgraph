//
//  Column.swift
//  TelegramGraph
//
//  Created by Alexandr Gaidukov on 13/03/2019.
//  Copyright © 2019 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum ColumnType: String, Decodable {
    case line
    case bar
    case area
    case x
}

func ==(lhs: Column, rhs: Column) -> Bool {
    return lhs.label == rhs.label
}

struct Column: Decodable, Hashable {
    var label: String
    var name: String!
    var values: [Int]
    var type: ColumnType!
    var color: UIColor!
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(label)
    }
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        var values: [Int] = []
        var label = ""
        if let count = container.count, count > 0 {
            values.reserveCapacity(count - 1)
        }
        while !container.isAtEnd {
            if label.isEmpty {
                label = try container.decode(String.self)
                continue
            }
            values.append(try container.decode(Int.self))
        }
        
        self.label = label
        self.values = values
    }
}
